<?php

/**
 * @file
 * Contains context_node_template.module..
 */

use Drupal\Component\Utility\Unicode;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\ContentEntityFormInterface;

/**
 * Implements hook_theme().
 */
function context_node_template_theme() {
  return array(
    'context_node_template_setting' => array(
      'render element' => 'form',
      'file' => 'context_node_template.admin.inc',
    ),
  );
}

/**
 * Scan the page templates in theme dir.
 */
function _get_page_templates() {
  $config = \Drupal::configFactory();
  $theme = $config->get('system.theme')->get('default');
  $theme_path = drupal_get_path('theme', $theme);
  $options = array('default' => 'default');

  $files = file_scan_directory($theme_path, '/.*\.html.twig$/', array('key' => 'name'));
  if (!empty($files)) {
    foreach ($files as $file) {
      if (Unicode::substr($file->name, 0, 4) == 'page') {
        $name = Unicode::substr($file->name, 0, -5);  // name without .tpl
        $options[$name] = context_node_template_get_templates_alias($name);
      }  
    }
  }

  return $options;
}

/**
 * Get alias by template name.
 */
function context_node_template_get_templates_alias($name) {
  $result = db_query('SELECT template_alias FROM {node_template_alias} WHERE template = :template', array(':template' => $name))->fetchObject();
  if(!empty($result->template_alias)) {
    return $result->template_alias;
  }

  return $name;
}

/**
 * Implementation of hook_theme_suggestions_HOOK_alter().
 */
function context_node_template_theme_suggestions_page_alter(array &$suggestions, array $variables) {
  $node = \Drupal::routeMatch()->getParameter('node');
  $request_uri = \Drupal::request()->getRequestUri();

  if (Unicode::substr($request_uri, -5) != '/edit' && !empty($node)) {
    $result = db_query('SELECT template FROM {node_template} WHERE nid = :nid', array(':nid' => $node->id()))->fetchObject();
    if(!empty($result->template)){
      $suggestions[] = strtr($result->template,"--","__");
    }
  }
}

/**
 * Implements hook_form_alter().
 */
function context_node_template_form_alter(array &$form, FormStateInterface $form_state, $form_id) {
  $form_object = $form_state->getFormObject();
  if (!($form_object instanceof ContentEntityFormInterface)) {
    return;
  }

  $node = $form_object->getEntity();
  $op = $form_object->getOperation();

  if($op == 'edit' || $op == 'default'){    
    $result = db_query('SELECT template FROM {node_template} WHERE nid = :nid', array(':nid' => $node->id()))->fetchObject();
  }


  if ('node_'.$node->getType().'_form' == $form_id || 'node_'.$node->getType().'_edit_form') {
    $user = \Drupal::currentUser();

    $templates = _get_page_templates();

    $form['node_template'] = array(
      '#type' => 'select',
      '#title' => 'Node Template',
      '#options' => $templates,
      '#default_value' => $op == 'edit' ? $result->template : 'default',
      '#description' => t('Select a template'),
      '#access' => $user->hasPermission('administer node templates'),
    );

    foreach (array_keys($form['actions']) as $action) {
      if ($action != 'preview' && isset($form['actions'][$action]['#type']) && $form['actions'][$action]['#type'] === 'submit') {
        $form['actions'][$action]['#submit'][] = 'context_node_template_node_form_submit';
      }
    }
  }
  
}

/**
 * Implements hook_ENTITY_TYPE_update() for node entities.
 */
function context_node_template_node_form_submit($form, FormStateInterface $form_state) {
  if(!empty($form_state->getValue('node_template'))){
    $result = db_query('SELECT template FROM {node_template} WHERE nid = :nid', array(':nid' => $form_state->getValue('nid')))->fetchObject();
  
    $template = $form_state->getValue('node_template');
    if(!empty($template)){
      if (empty($result->template)) {
        db_insert('node_template')
          ->fields(array(
            'nid' => $form_state->getValue('nid'),
            'template' => $template,
          ))
          ->execute();
      }
      else {
        db_update('node_template')
          ->fields(array('template' => $template))
          ->condition('nid', $form_state->getValue('nid'))
          ->execute();
      }
    }else{
      db_delete('node_template')
        ->condition('nid', $form_state->getValue('nid'))
        ->execute();
    }
  }
}
